Nikoloz Aneli
Developer
- Completed a Bachelor's degree in Computer Science.
- Developed a personal website using HTML, CSS, and JavaScript.
- Contributed to an open-source project on GitHub.